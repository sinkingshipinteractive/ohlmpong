/* global require, ss */
require.include("namespaces/OhlmPongNamespace");

( function() {
	"use strict";

	/**
	 * Assets required for the preloader scene for the ohlmpong game.
	 * @type {Object}
	 */
	ss.ohlmpong.audioAssets = [
			{ "id" : "music-loop",					"src" : "audio/music-play-loop", "tags": ["music"] },
			{ "id" : "sfx-ball-spawn",				"src" : "audio/sfx-ball-spawn", "tags": ["sfx"] },
			{ "id" : "sfx-miss-out",				"src" : "audio/sfx-miss-out", "tags": ["sfx"] },
			{ "id" : "sfx-ohlm-appear",				"src" : "audio/sfx-ohlm-appear", "tags": ["sfx"] },
			{ "id" : "sfx-pan-hit",					"src" : "audio/sfx-pan-hit", "tags": ["sfx"] },
			{ "id" : "sfx-tray-hit",				"src" : "audio/sfx-tray-hit", "tags": ["sfx"] },
			{ "id" : "sfx-wall-hit",				"src" : "audio/sfx-wall-hit", "tags": ["sfx"] },
			{ "id" : "OT_PP_Alan",					"src" : "audio/vo/OT_PP_Alan", "tags": ["vo"] },
			{ "id" : "OT_PP_AnotherPan",			"src" : "audio/vo/OT_PP_AnotherPan", "tags": ["vo"] },
			{ "id" : "OT_PP_BeatMallsMouncing",		"src" : "audio/vo/OT_PP_BeatMallsMouncing", "tags": ["vo"] },
			{ "id" : "OT_PP_BeatMallsTryAgain",		"src" : "audio/vo/OT_PP_BeatMallsTryAgain", "tags": ["vo"] },
			{ "id" : "OT_PP_BigPan",				"src" : "audio/vo/OT_PP_BigPan", "tags": ["vo"] },
			{ "id" : "OT_PP_CakePan",				"src" : "audio/vo/OT_PP_CakePan", "tags": ["vo"] },
			{ "id" : "OT_PP_Edges",					"src" : "audio/vo/OT_PP_Edges", "tags": ["vo"] },
			{ "id" : "OT_PP_GoodWork",				"src" : "audio/vo/OT_PP_GoodWork", "tags": ["vo"] },
			{ "id" : "OT_PP_GuessWhat",				"src" : "audio/vo/OT_PP_GuessWhat", "tags": ["vo"] },
			{ "id" : "OT_PP_HadToBounce",			"src" : "audio/vo/OT_PP_HadToBounce", "tags": ["vo"] },
			{ "id" : "OT_PP_HongKong",				"src" : "audio/vo/OT_PP_HongKong", "tags": ["vo"] },
			{ "id" : "OT_PP_ILovePans",				"src" : "audio/vo/OT_PP_ILovePans", "tags": ["vo"] },
			{ "id" : "OT_PP_Intro",					"src" : "audio/vo/OT_PP_Intro", "tags": ["vo"] },
			{ "id" : "OT_PP_KeepEmBouncing",		"src" : "audio/vo/OT_PP_KeepEmBouncing", "tags": ["vo"] },
			{ "id" : "OT_PP_KeepThemInAir",			"src" : "audio/vo/OT_PP_KeepThemInAir", "tags": ["vo"] },
			{ "id" : "OT_PP_Lemons",				"src" : "audio/vo/OT_PP_Lemons", "tags": ["vo"] },
			{ "id" : "OT_PP_LottaMeatballs",		"src" : "audio/vo/OT_PP_LottaMeatballs", "tags": ["vo"] },
			{ "id" : "OT_PP_LoveHeartPan",			"src" : "audio/vo/OT_PP_LoveHeartPan", "tags": ["vo"] },
			{ "id" : "OT_PP_MeatBallsTryAgain",		"src" : "audio/vo/OT_PP_MeatBallsTryAgain", "tags": ["vo"] },
			{ "id" : "OT_PP_MorePans",				"src" : "audio/vo/OT_PP_MorePans", "tags": ["vo"] },
			{ "id" : "OT_PP_NotDistracting",		"src" : "audio/vo/OT_PP_NotDistracting", "tags": ["vo"] },
			{ "id" : "OT_PP_OneDay",				"src" : "audio/vo/OT_PP_OneDay", "tags": ["vo"] },
			{ "id" : "OT_PP_Pan",					"src" : "audio/vo/OT_PP_Pan", "tags": ["vo"] },
			{ "id" : "OT_PP_PanIsSquare",			"src" : "audio/vo/OT_PP_PanIsSquare", "tags": ["vo"] },
			{ "id" : "OT_PP_Peekaboo",				"src" : "audio/vo/OT_PP_Peekaboo", "tags": ["vo"] },
			{ "id" : "OT_PP_RectangularPan",		"src" : "audio/vo/OT_PP_RectangularPan", "tags": ["vo"] },
			{ "id" : "OT_PP_RoundPan",				"src" : "audio/vo/OT_PP_RoundPan", "tags": ["vo"] },
			{ "id" : "OT_PP_SaucePan4Sauce",		"src" : "audio/vo/OT_PP_SaucePan4Sauce", "tags": ["vo"] },
			{ "id" : "OT_PP_SquarePan",				"src" : "audio/vo/OT_PP_SquarePan", "tags": ["vo"] },
			{ "id" : "OT_PP_ThatsIt",				"src" : "audio/vo/OT_PP_ThatsIt", "tags": ["vo"] },
			{ "id" : "OT_PP_ThatsIt_OhlmItUp",		"src" : "audio/vo/OT_PP_ThatsIt_OhlmItUp", "tags": ["vo"] },
			{ "id" : "OT_PP_ThatsItHowOhlmy",		"src" : "audio/vo/OT_PP_ThatsItHowOhlmy", "tags": ["vo"] },
			{ "id" : "OT_PP_ThisIsRoundPan",		"src" : "audio/vo/OT_PP_ThisIsRoundPan", "tags": ["vo"] },
			{ "id" : "OT_PP_ThisIsSaucePan",		"src" : "audio/vo/OT_PP_ThisIsSaucePan", "tags": ["vo"] }
		];
} ());



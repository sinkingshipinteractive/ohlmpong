/* global require, ss */
require.include("namespaces/OhlmPongNamespace");

( function() {
	"use strict";

	/**
	 * Assets required for the gameplay scene for the ohlmpong game.
	 * @type {Object}
	 */
	ss.ohlmpong.gameAssets = {
		assets : [
			{ "id" : "Pot",				"src" : "images/Pot.png" },
			{ "id" : "PotFront",		"src" : "images/Pot_front.png" },
			{ "id" : "Meatball",		"src" : "images/Meatball.png" },
			{ "id" : "Bowl",			"src" : "images/Bowl.png" },
			{ "id" : "Background",		"src" : "images/Background.jpg" },
			{ "id" : "CounterTop",		"src" : "images/CounterTop.png" },
			{ "id" : "HighScoreBG",     "src" : "images/HighScoreBG.png" },
			{ "id" : "Numbers",			"src" : "images/Numbers.json"},
			{ "id" : "Splat",			"src" : "images/Splat.json"},
			{ "id" : "SpawnImage",		"src" : "images/SpawnImage.png"},
			{ "id" : "TryAgain",		"src" : "images/TRYAGAIN.png"},
			{ "id" : "Ohlm_01",			"src" : "images/Ohlm_01.png" },
			{ "id" : "Ohlm_02",			"src" : "images/Ohlm_02.png" },
			{ "id" : "Ohlm_03",			"src" : "images/Ohlm_03.png" },
			{ "id" : "Ohlm_04",			"src" : "images/Ohlm_04.png" },
			{ "id" : "Ohlm_05",			"src" : "images/Ohlm_05.png" },
			{ "id" : "Ohlm_06",			"src" : "images/Ohlm_06.png" },
			{ "id" : "Ohlm_07",			"src" : "images/Ohlm_07.png" }
		]
	};

} ());


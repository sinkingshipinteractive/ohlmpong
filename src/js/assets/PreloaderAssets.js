/* global require, ss */
require.include("namespaces/OhlmPongNamespace");

( function() {
	"use strict";

	/**
	 * Assets required for the preloader scene for the ohlmpong game.
	 * @type {Object}
	 */
	ss.ohlmpong.preloaderAssets = {
		assets : [
			{ "id" : "LoadingBg",			"src" : "images/preloader/Loading_background.png" },
			{ "id" : "LoadingFg",			"src" : "images/preloader/Loading_Fill.png" }
		]
	};

} ());

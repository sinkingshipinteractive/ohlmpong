/* global require, ss */

require.include("namespaces/OhlmPongNamespace.js");

( function() {

	/*
	* Global game configuration for sorting game.
	*/
	ss.ohlmpong.GameConfig = ss.ohlmpong.GameConfig || new function GameConfig() { // jshint ignore:line
		"use strict";

/* Private vars */

		// {Boolean} whether or not we're in debug mode, so show debug graphics / text in the game.
		var DEBUG_MODE = false;

		// {ss.compubot.GameConfig} create a locally scoped copy of "this".
		var _this = this;

		// {Number} The width of the game in pixels.
		var GAME_WIDTH = 1024;

		// {Number} The height of the game in pixels.
		var GAME_HEIGHT = 768;

		// {Number} how long each tick should take in ms.
		var TICK_TIME = 15;

/* Public vars */

		/*
		* Get the width of the game in pixels.
		*/
		_this.getGameWidth = function() {
			return GAME_WIDTH;
		};

		/*
		* Get the height of the game in pixels.
		*/
		_this.getGameHeight = function() {
			return GAME_HEIGHT;
		};

		/*
		* Get the length of each tick in ms.
		*/
		_this.getTickTime = function() {
			return TICK_TIME;
		};

		/*
		* Get whether or not we're in debug mode.
		*/
		_this.getDebugMode = function() {
			return DEBUG_MODE;
		};

	}; // jshint ignore:line

}());

/* global require, ss, $, console */
require.config ( {
	"baseUrl" :		"js",
	"alias": {"core" : [
                       "weblib/asset/CreateJSAssetManager",
                       "weblib/pools/PoolManager",
                       "weblib/command/CommandTree",
                       "weblib/command/CommandSet",
                       "local-namespace",
                       "weblib/sound/SoundPackage"
                       ]},
	"ignore": [],
	"include": [
				],
	"debug"   : 	true,
	"synced"  : 	true,
	"useFullPath": true
} );
require.include("components/easeljs/lib/easeljs-NEXT.combined.js");
require.include("components/SoundJS/lib/soundjs-NEXT.combined.js");
require.include("components/SoundJS/lib/flashaudioplugin-NEXT.combined.js");
require.include("components/TweenJS/lib/tweenjs-NEXT.combined.js");
require.include("components/PreloadJS/lib/preloadjs-NEXT.combined.js");
require.include("components/jquery/dist/jquery.js");
require.include("GameManager.js");

(function () {
	"use strict";

	var _gameManager;

	/**
	 * Used to generate font size on body for em dynamic rezing on screen size change
	 */
	$.fn.flowtype = function(options) {

	  var settings = $.extend({
		 maximum   : 9999,
		 minimum   : 1,
		 maxFont   : 9999,
		 minFont   : 1,
		 fontRatio : 30
	  }, options),

	  changes = function(el) {
		 var $el = $(el),
			elw = $el.width(),
			width = elw > settings.maximum ? settings.maximum : elw < settings.minimum ? settings.minimum : elw,
			fontBase = width / settings.fontRatio,
			fontSize = fontBase > settings.maxFont ? settings.maxFont : fontBase < settings.minFont ? settings.minFont : fontBase;
		 $el.css('font-size', fontSize + 'px');
	  };

	  return this.each(function() {
	  // Context for resize callback
		 var that = this;
	  // Make changes upon resize
		 $(window).resize(function(){changes(that);});
	  // Set changes on load
		 changes(this);
	  });
	};

	require.ready(function()
	{
		var $canvas = $( "canvas#game" );

		_gameManager = new ss.ohlmpong.GameManager($canvas.get(0));
		_gameManager.startGame();

		setTimeout(_resize, 100);

		$('body').flowtype({
			minimum   : 100,
			maximum   : 1500,
			fontRatio : 40
		});

		sizeGameFrame("#game");
	});


		function _resize() {
			sizeGameFrame("#game");
		}

var winSize;

function getWinSize() {
	return winSize = {
		x:$(window).width(),
		y:$(window).height()
	};
};


function sizeGameFrame(containerId){

	getWinSize();

	var gameNavSize = {
		y: $('.gameNav').outerHeight(true) + 40,
		x: $('.gameNav').outerWidth(true)
	};

	console.log('gameNavSize: ', gameNavSize);
	console.log('winSize: ', winSize);

	// 1024 x 768 (height to width) ratio: 133.33333333333333333333333333333
	// 1024 x 768 (width to height) ratio: 75
	var ratio = {
		y: 1.3333333333333333333333333333333,
		x: 0.75
	};

	var gameSize = {

		y: (winSize.y - gameNavSize.y) / 100 * 98,
		x: ((winSize.y - gameNavSize.y) / 100 * 98) * ratio.y

	};

	if(gameSize.x > winSize.x) {

		gameSize = {
			y: winSize.x * ratio.x,
			x: winSize.x
		};

	};

	console.log('gameSize: ', gameSize);

	$(containerId).css({
	});

	$(containerId).css({
		top: ((gameNavSize.y / 2) + (winSize.y / 2) - 40) + 'px',
		width: gameSize.x + 'px',
		height: gameSize.y + 'px'
	});

	};


	$ ( window ).resize ( _resize );
}()
);
	/* global require, createjs, ss*/
require.include("namespaces/OhlmPongNamespace.js");
require.include("weblib/scene/ScenePackage");
require.include("scenes/SceneFactory");
require.include("config/GameConfig");

( function() {
		"use strict";

	function GameManager (canvas) {

		// locally scoped copy of "this".
		var _this = this;

		// Canvas to draw the game scene to.
		var _canvas;

		// CreateJS stage to draw the content to.
		var _stage;

		// Container to hod the scene that being displayed.
		var _sceneContainer;

		// manager to keep track of scenes in the game.
		var _sceneManager;

		var _muted = false;

		function _construct (canvas) {
			// setup the game canvas.
			_canvas = canvas;

			// setup the game stage.
			_stage = new createjs.Stage(_canvas);

			// create a container to hold the scenes and add it to the stage.
			_sceneContainer = new createjs.Container();
			_stage.addChild(_sceneContainer);

			// enable touch & mouse over
			createjs.Touch.enable(_stage, true);
		//	_stage.enableMouseOver();

			return _this;
		}

		/*
		* Start the game.
		*/
		_this.startGame = function() {
			$(".btn-game-toggleMute").click(_toggleMute);
			$(".btn-game-toggleCaptions").removeClass("unmuted");
			$(".btn-game-toggleCaptions").addClass("muted");

			_sceneManager = new ss.TransitionSceneManager(_sceneContainer, new ss.ohlmpong.SceneFactory(), ss.ohlmpong.GameConfig.getGameWidth(), ss.ohlmpong.GameConfig.getGameHeight());
			_sceneManager.gotoScene(ss.ohlmpong.SceneType.PRELOAD_SCENE, null);

			_startGameLoop();
		};

		function _toggleMute() {
			if(_muted) {
				ss.AudioManager.unmuteAll();
				_muted = false;
				$(".btn-game-toggleMute").removeClass("muted");
				$(".btn-game-toggleMute").addClass("unmuted");
			} else {
				ss.AudioManager.muteAll();
				_muted = true;
				$(".btn-game-toggleMute").removeClass("unmuted");
				$(".btn-game-toggleMute").addClass("muted");
			}
		}

		/*
		* Start running the game loop.
		*/
		function _startGameLoop () {
			// createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
			createjs.Ticker.setInterval(ss.ohlmpong.GameConfig.getTickTime());
			createjs.Ticker.addEventListener("tick", _handleGameTick);
		}

		function _handleGameTick (tickEvent) {
			var deltaSeconds = tickEvent.delta / 1000.0;

			_stage.update(tickEvent);
			_sceneManager.update(deltaSeconds);
		}

		return _construct(canvas);
	}

	ss.ohlmpong.GameManager = GameManager;
}());

/* global require, createjs, ss */

(function () {
	"use strict";

	FlameLayer.prototype = new createjs.Container();
	FlameLayer.prototype.constructor = FlameLayer;

	/**
	 *
	 */
	function FlameLayer() {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		var _flame1;
		var _flame2;
		var _flame3;
		var _flame4;

		/**
		 * Start this instance.
		 */
		_this.start = function () {
			_flame1 = ss.CreateJSAssetManager.getBitmap("FlameBack");
			_flame1.regX = _flame1.width / 2;
			_flame1.regY = _flame1.height - 50;
			_flame1.y = 670;
			_flame1.x = 322;
			_this.addChild(_flame1);

			_flame2 = ss.CreateJSAssetManager.getBitmap("FlameBack");
			_flame2.regX = _flame2.width / 2;
			_flame2.regY = _flame2.height - 50;
			_flame2.y = 670;
			_flame2.x = 695;
			_this.addChild(_flame2);

			_flame3 = ss.CreateJSAssetManager.getBitmap("FlameFront");
			_flame3.regX = _flame3.width / 2;
			_flame3.regY = _flame3.height - 50;
			_flame3.y = 700;
			_flame3.x = 295;
			_this.addChild(_flame3);

			_flame4 = ss.CreateJSAssetManager.getBitmap("FlameFront");
			_flame4.regX = _flame4.width / 2;
			_flame4.regY = _flame4.height - 50;
			_flame4.y = 700;
			_flame4.x = 720;
			_this.addChild(_flame4);
		};

		_this.update = function (delta) {
			_flame1.scaleY = Math.random() * 0.25 + 0.75;
			_flame1.scaleX = Math.random() * 0.25 + 0.75;

			_flame2.scaleY = Math.random() * 0.25 + 0.75;
			_flame2.scaleX = Math.random() * 0.25 + 0.75;

			_flame3.scaleY = Math.random() * 0.25 + 0.75;
			_flame3.scaleX = Math.random() * 0.25 + 0.75;

			_flame4.scaleY = Math.random() * 0.25 + 0.75;
			_flame4.scaleX = Math.random() * 0.25 + 0.75;
		};


		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function () {
			_this = undefined;
		};
	}

	ss.ohlmpong.FlameLayer = FlameLayer;

}());


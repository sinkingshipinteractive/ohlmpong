/* global require, createjs, ss */

(function () {
	"use strict";

	Flame.prototype = new createjs.Container();
	Flame.prototype.constructor = Flame;

	/**
	 * Burner flame effect for meatball bouncing game.
	 */
	function Flame() {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		var _flames;

		/**
		 * Start this instance.
		 */
		_this.start = function () {
			_flames = [];

			for (var i = 150; i <890; i+= ss.MathUtils.randomRange(10, 20)) {
				var newFlame = ss.CreateJSAssetManager.getBitmap("Flame");
				_this.addChild(newFlame);
				newFlame.regX = newFlame.width / 2;
				newFlame.regY = 42;
				newFlame.x = i;
				newFlame.y = 775;
				newFlame.alpha = 0.35;
				newFlame.scaleY = ss.MathUtils.randomRange(0.5, 1.5);
				newFlame.scaleX = newFlame.scaleY;
				newFlame.compositeOperation = "lighter";
				_flames.push(newFlame);
			}
		};

		/**
		 * Update the flames.
		 * @param  {Number} delta - The time since the last frame, in seconds.
		 * @param  {Number} panX  - The x coordinate of the pan's position on screen, in pixels.
		 */
		_this.update = function (delta, panX) {
			for (var i =0; i < _flames.length; i++) {
				if(Math.abs(_flames[i].x - panX) < 180) {
					_flames[i].targetRotation = ss.MathUtils.clamp((90 - (panX - _flames[i].x) * 0.5), -60, 60);
				} else {
					_flames[i].targetRotation = 0;
				}
				_flames[i].targetRotation += ss.MathUtils.randomRange(-50, 50);

				_flames[i].rotation = ss.MathUtils.lerp1D(_flames[i].rotation, _flames[i].targetRotation, 3* delta);

				_flames[i].scaleY += ss.MathUtils.randomRange(-0.1, 0.1);
				_flames[i].scaleY = ss.MathUtils.lerp1D(_flames[i].scaleY, 1.5, 5 * delta);
				_flames[i].scaleX = _flames[i].scaleY * 1.25;
			}
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function () {
			_this = undefined;
		};
	}

	ss.ohlmpong.Flame = Flame;

}());


/* global require, createjs, ss */

(function () {
	"use strict";

	NumberDisplay.prototype = new createjs.Container();
	NumberDisplay.prototype.constructor = NumberDisplay;

	/**
	 *
	 */
	function NumberDisplay() {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;
		var _digits;
		var _prefix;
		var _center;

		/**
		 * Start this instance.
		 */
		_this.start = function (value, prefix, center) {
			_prefix = prefix;
			_center = center;
			_digits = [];
			_this.setText(value);
		};

		_this.setText = function(newVal) {
			for (var i =0; i < _digits.length; i++) {
				_this.removeChild(_digits[i]);
			}
			_digits = [];

			var valString = newVal.toString();
			var totalWidth = 0;
			var h;
			for (i = 0; i < valString.length; i++) {
				var character = valString[i];
				var newNum = ss.CreateJSAssetManager.getSprite(_prefix + character);
				newNum.x = totalWidth;
				totalWidth += newNum.width;
				h = newNum.height;
				_this.addChild(newNum);
				_digits.push(newNum);
			}

			// center it!
			if(_center) {
				_this.regX = totalWidth / 2;
				_this.regY = h / 2;
			}
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function () {
			for (var i =0; i < _digits.length; i++) {
				_this.removeChild(_digits[i]);
			}
			_digits = undefined;
			_this = undefined;
		};
	}

	ss.ohlmpong.NumberDisplay = NumberDisplay;

}());


/* global require, createjs, ss */

require.include("weblib/collection/ShuffledRandomizer");

(function () {
	"use strict";

	OhlmLayer.prototype = new createjs.Container();
	OhlmLayer.prototype.constructor = OhlmLayer;

	/**
	 *
	 */
	function OhlmLayer(captionDisplay) {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;




		var _ohlms = [];
		var _possibleOhlms = ["Ohlm_01","Ohlm_02","Ohlm_03","Ohlm_04","Ohlm_05","Ohlm_06","Ohlm_07"];
		var _genericAudioClips = [
			{
				"vo": "OT_PP_Alan",
			 	"captions": [
			 		{ text: "Great Job! My friend Alan would be impressed. You know, the pigeon." }
			 	]
			},
			{
				"vo": "OT_PP_AnotherPan",
			 	"captions": [
			 		{ text: "Another pan." }
			 	]
			},
			{
				"vo": "OT_PP_Edges",
			 	"captions": [
			 		{ text: "I love pans, especially the edges." }
			 	]
			},
			{
				"vo": "OT_PP_GoodWork",
			 	"captions": [
			 		{ text: "Good work!" }
			 	]
			},
			{
				"vo": "OT_PP_GuessWhat",
			 	"captions": [
			 		{ text: "Guess what? Pan." }
			 	]
			},
			{
				"vo": "OT_PP_HongKong",
			 	"captions": [
			 		{ text: "I got all these pans in Hong Kong." }
			 	]
			},
			{
				"vo": "OT_PP_ILovePans",
			 	"captions": [
			 		{ text: "I love pans." }
			 	]
			},
			{
				"vo": "OT_PP_Lemons",
			 	"captions": [
			 		{ text: "If life gives you lemons, hand them back and say no thank you." }
			 	]
			},{
				"vo": "OT_PP_LottaMeatballs",
			 	"captions": [
			 		{ text: "That's a lot of meatballs!" }
			 	]
			},{
				"vo": "OT_PP_LottaMeatballs",
			 	"captions": [
			 		{ text: "That's a lot of meatballs!" }
			 	]
			}, {
				"vo": "OT_PP_MorePans",
			 	"captions": [
			 		{ text: "More pans." }
			 	]
			}, {
				"vo": "OT_PP_NotDistracting",
			 	"captions": [
			 		{ text: "I hope I'm not distracting you with all these pans." }
			 	]
			}, {
				"vo": "OT_PP_OneDay",
			 	"captions": [
			 		{ text: "One day, Odd Squad will be all mine." }
			 	]
			}, {
				"vo": "OT_PP_Peekaboo",
			 	"captions": [
			 		{ text: "Peekaboo! It's a pan" }
			 	]
			}, {
				"vo": "OT_PP_Pan",
			 	"captions": [
			 		{ text: "Pan!" }
			 	]
			}, {
				"vo": "OT_PP_ThatsIt",
			 	"captions": [
			 		{ text: "That's it!" }
			 	]
			}, {
				"vo": "OT_PP_ThatsIt_OhlmItUp",
			 	"captions": [
			 		{ text: "That's it, Ohlm it up!" }
			 	]
			}, {
				"vo": "OT_PP_ThatsItHowOhlmy",
			 	"captions": [
			 		{ text: "That's it, how Ohlmy." }
			 	]
			}
		];

		var _specificAudioClips = {
			"Ohlm_01" : [// Ohlm_01: Heart Pan
				{
					"vo": "OT_PP_LoveHeartPan",
			 		"captions": [
			 			{ text: "I love my heart shaped pan." }
			 		]
				}
			],
			"Ohlm_02": [ // Ohlm_02: Big Pan
				{
					"vo": "OT_PP_BigPan",
			 		"captions": [
			 			{ text: "Big pan." }
			 		]
				},
				{
					"vo": "OT_PP_RectangularPan",
			 		"captions": [
			 			{ text: "Rectangular pan." }
			 		]
				}

			],
			"Ohlm_03": [ // Ohlm_03: Sauce Pan
				{
					"vo": "OT_PP_SaucePan4Sauce",
			 		"captions": [
			 			{ text: "Sauce pan. It's a pan, for sauce." }
			 		]
				},
				{
					"vo": "OT_PP_ThisIsSaucePan",
			 		"captions": [
			 			{ text: "This is my sauce pan." }
			 		]
				}
			],
			"Ohlm_04": [ // Ohlm_04: Round Pan / Cake Pan
				{
					"vo": "OT_PP_CakePan",
			 		"captions": [
			 			{ text: "Cake pan." }
			 		]
				},
				{
					"vo": "OT_PP_RoundPan",
			 		"captions": [
			 			{ text: "Round pan." }
			 		]
				},
				{
					"vo": "OT_PP_ThisIsRoundPan",
			 		"captions": [
			 			{ text: "This is my round pan." }
			 		]
				}
			],
			"Ohlm_05": [ // Ohlm_05: Big Pan
				{
					"vo": "OT_PP_BigPan",
			 		"captions": [
			 			{ text: "Big pan." }
			 		]
				},
				{
					"vo": "OT_PP_RectangularPan",
			 		"captions": [
			 			{ text: "Rectangular pan." }
			 		]
				}
			],
			"Ohlm_06": [ // Ohlm_06: Square Pan
				{
					"vo": "OT_PP_PanIsSquare",
			 		"captions": [
			 			{ text: "This pan is square." }
			 		]
				},
				{
					"vo": "OT_PP_SquarePan",
			 		"captions": [
			 			{ text: "Square pan." }
			 		]
				}
			],
			"Ohlm_07": [ // Ohlm_07: Round Pan / Cake Pan
				{
					"vo": "OT_PP_CakePan",
			 		"captions": [
			 			{ text: "Cake pan." }
			 		]
				},
				{
					"vo": "OT_PP_RoundPan",
			 		"captions": [
			 			{ text: "Round pan." }
			 		]
				},
				{
					"vo": "OT_PP_ThisIsRoundPan",
			 		"captions": [
			 			{ text: "This is my round pan." }
			 		]
				}
			]
		};

		var _ohlmRandomizer;

		var _captionDisplay;
		/**
		 * Start this instance.
		 */
		function _construct(captionDisplay) {
			_captionDisplay = captionDisplay;
			_ohlmRandomizer = new ss.ShuffledRandomizer(_possibleOhlms);
		}

		_this.showOhlm = function () {
			if(ss.AudioManager.getCurrentlyPlayingSoundsByTag("vo").length > 0) {
				return;
			}



			// _genericAudioClips
			// _specificAudioClips

			var nextOhlm = _ohlmRandomizer.getNextItem();

			var audioOptions;
			if(Math.random() >= 0.5) {
				audioOptions = _genericAudioClips;
			} else {
				audioOptions = _specificAudioClips[nextOhlm];
			}

			console.dir(audioOptions);
			var audioChoice = audioOptions[Math.floor(Math.random() * audioOptions.length)];
			console.dir(audioChoice);
			_captionDisplay.playScript(audioChoice.vo, audioChoice.captions);

			var newOhlm = ss.CreateJSAssetManager.getBitmap(nextOhlm);
			newOhlm.regX = newOhlm.width / 2;
			newOhlm.regY = newOhlm.height;
			newOhlm.y = 768+250;
			newOhlm.x = ss.MathUtils.randomRange(150, 1024-150);
			createjs.Tween.get(newOhlm).to({y: 675}, 200).wait(1500).to({y:1030}, 200);
			_this.addChild(newOhlm);
			_ohlms.push(newOhlm);
		};


		_this.update = function () {
		 	for (var i = _ohlms.length - 1; i >= 0; i--) {
		 		if(_ohlms[i].y >= 1025) {
		 			_this.removeChild(_ohlms[i]);
		 			_ohlms.splice(i, 1);
		 		}
		 	}
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function () {
			_this = undefined;
		};

		return _construct(captionDisplay);
	}

	ss.ohlmpong.OhlmLayer = OhlmLayer;

}());


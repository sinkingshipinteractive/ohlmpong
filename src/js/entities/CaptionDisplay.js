/* global require, createjs, ss */

(function () {
	"use strict";

	CaptionDisplay.prototype = new createjs.Container();
	CaptionDisplay.prototype.constructor = CaptionDisplay;

	/**
	 *
	 */
	function CaptionDisplay() {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		var _captionText;


		var _currentLineIndex;
		var _currentScript;

		var _captionsEnabled;

		/**
		 * Start this instance.
		 */
		_this.start = function () {
			_captionText = new createjs.Text("", "bold 37px refrigerator-deluxe", "#ffffff");
			_captionText.textAlign = "center";
			_captionText.x = 512;
			_captionText.y = 720;
			_captionText.visible = false;
			var shadow = new createjs.Shadow("#000000", 0, 0, 10);
			_captionText.shadow = shadow;
			_this.addChild(_captionText);
			_captionsEnabled = false;

			$(".btn-game-toggleCaptions").click(_toggleCaptions);

		};


		function _toggleCaptions() {
			if(_captionsEnabled) {
				_captionText.visible = false;
				_captionsEnabled = false;
				$(".btn-game-toggleCaptions").removeClass("unmuted");
				$(".btn-game-toggleCaptions").addClass("muted");
			} else {
				_captionText.visible = true;
				_captionsEnabled = true;
				$(".btn-game-toggleCaptions").removeClass("muted");
				$(".btn-game-toggleCaptions").addClass("unmuted");
			}
		}

		_this.playScript = function(voLine, captions) {
			_currentLineIndex = 0;
			_currentScript = captions;
			ss.AudioManager.play(voLine, _clearCaption);
			createjs.Tween.removeTweens(_captionText);

			_playNextLine();
		};

		function _playNextLine() {
			if(_currentLineIndex == _currentLineIndex.length - 1) {
				return;
			}
			var currentCaption = _currentScript[_currentLineIndex];
			if(ss.isEmpty(currentCaption)) {
				return;
			}
			_captionText.text = currentCaption.text;
			_captionText.scaleX = 0;
			_captionText.scaleY = 0;

			var tween = createjs.Tween.get(_captionText);
			tween.to({scaleX: 1, scaleY: 1}, 100);
			tween.wait(currentCaption.time);
			_currentLineIndex++;
			tween.call(_playNextLine);
		}

		function _clearCaption () {
			createjs.Tween.get(_captionText).to({scaleX: 0, scaleY: 0}, 100);
		}

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function () {
			_this = undefined;
		};
	}

	ss.ohlmpong.CaptionDisplay = CaptionDisplay;

}());


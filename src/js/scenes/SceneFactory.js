/* global require, ss */
require.include("namespaces/OhlmPongNamespace.js");
require.include("scenes/PreloadScene");
require.include("scenes/GameScene");

(function() {
	"use strict";

	SceneFactory.prototype = {};
	SceneFactory.prototype.constructor = SceneFactory;

	/**
	 *
	 */
	function SceneFactory () {
		// keep a locally scoped copy of this
		var _this = this;

		_this.construct = function() {
			return _this;
		};

		_this.createSceneByName = function(sceneName, sceneData) {
			var width = ss.ohlmpong.GameConfig.getGameWidth();
			var height = ss.ohlmpong.GameConfig.getGameHeight();

			switch(sceneName) {
				case SceneType.PRELOAD_SCENE:
					return new ss.ohlmpong.PreloadScene(width, height, sceneData);
				case SceneType.GAME_SCENE:
					return new ss.ohlmpong.GameScene(width, height, sceneData);
			}
		};

		return _this.construct();
	}

	var SceneType = {};
	SceneType.PRELOAD_SCENE = "PRELOAD_SCENE";
	SceneType.GAME_SCENE = "GAME_SCENE";

	ss.ohlmpong.SceneFactory = SceneFactory;
	ss.ohlmpong.SceneType = SceneType;
}());

/* global require, ss, soundsSpriteJSON */

require.include("namespaces/OhlmPongNamespace.js");
require.include("assets/PreloaderAssets");
require.include("assets/GameAssets");
require.include("config/GameConfig");
require.include("assets/AudioAssets");
require.include("weblib/audio/AudioPackage");
require.include("weblib/asset/AssetLoaderEvent");
require.include("weblib/asset/CreateJSAssetManager");
require.include("weblib/ui/percentdisplays/TextProgressIndicator");
require.include("weblib/ui/percentdisplays/ImageProgressIndicator");
require.include("weblib/logging/Log");
require.include("weblib/scene/AbstractScene");
require.include("weblib/scene/SceneEvent");

(function() {
	"use strict";

	PreloadScene.prototype = new ss.AbstractScene();
	PreloadScene.prototype.constructor = PreloadScene;

	/*
	* Scene containing a preloader.
	* @param gameWidth: [Number] The width of the game in pixels.
	* @param gameHeight: [Number] The height of the game in pixels.
	*/
	function PreloadScene (gameWidth, gameHeight) {
		// The x center of the progess indicator.
		var PROGRESS_INDICATOR_X = 512;
		// The y center of the progress indicator.
		var PROGRESS_INDICATOR_Y = 400;

		// extend abstract scene, and keep a locally scoped copy of this.
		ss.AbstractScene.call(this, gameWidth, gameHeight);

		// keep a locally scoped copy of this.
		var _this = this;

		// the asset manager.
		var _assetManager;

		// the progress indicator.
		var _progressIndicator;


		/*
		* Start up the scene.
		*/
		_this.startScene = function() {
			_this.startPreload();
		};

		/*
		* Start preloading.
		*/
		_this.startPreload = function() {
			_assetManager = ss.CreateJSAssetManager;
			_loadPreloaderAssets();
		};

		_this.loadAudio = function() {
			ss.AudioManager.load(ss.ohlmpong.audioAssets, _handleSoundLoaded);
		};

		function _handleSoundLoaded () {
			_progressIndicator.displayPercent(1);

			var sceneData = {
			};

			_this.dispatchEvent(new ss.SceneEvent(ss.SceneEvent.GOTO_SCENE, { sceneName : ss.ohlmpong.SceneType.GAME_SCENE, sceneData : sceneData }));
		}

		/*
		* Load the assets needed for the preloader.
		*/
		function _loadPreloaderAssets () {
			_assetManager.addEventListener(ss.AssetLoaderEvent.LOAD_COMPLETE, _handlePreloaderLoaded);

			// load the preloader assets, definied in /config/PreloaderAssets.js
			_assetManager.load(ss.ohlmpong.preloaderAssets);
		}

		/*
		* Handle load complete on the preloader assets.
		*/
		function _handlePreloaderLoaded ()
		{
			// TODO: make this configurable.

			_assetManager.removeEventListener(ss.AssetLoaderEvent.LOAD_COMPLETE, _handlePreloaderLoaded);

			_progressIndicator = new ss.ImageProgressIndicator("LoadingBg", "LoadingFg", 316.5, 121);
			_this.addChild(_progressIndicator);
			_progressIndicator.x = PROGRESS_INDICATOR_X;
			_progressIndicator.y = PROGRESS_INDICATOR_Y;

			_loadGameAssets();

		}

		/*
		* Load the game assets themselves.
		*/
		function _loadGameAssets () {
			_assetManager.addEventListener(ss.AssetLoaderEvent.LOAD_COMPLETE, _handleLoadComplete);
			_assetManager.addEventListener(ss.AssetLoaderEvent.FILE_LOADED, _handleFileLoaded);
			_assetManager.load(ss.ohlmpong.gameAssets);
		}

		/*
		* Handle an individual file being loaded.
		*/
		function _handleFileLoaded () {
			var loadRatio = _assetManager.getNumLoadedAssets() / _assetManager.getTotalLoadingAssets();

			// Update the progress indicator
			_progressIndicator.displayPercent(loadRatio);
		}

		/*
		* Handle load completed.
		*/
		function _handleLoadComplete () {
			_assetManager.removeEventListener(ss.AssetLoaderEvent.LOAD_COMPLETE, _handleLoadComplete);
			_assetManager.removeEventListener(ss.AssetLoaderEvent.FILE_LOADED, _handleFileLoaded);

			_this.loadAudio();

		}

		/*
		* Destroy the scene.
		*/
		_this.destroyScene = function() {
			_this.release();
		};

		/**
		 * Release any assets held by this scene.
		 */
		this.release = function() {
			_progressIndicator.release();

			// remove everything from the scene.
			_this.removeChild(_progressIndicator);

			_assetManager = undefined;
	 		_progressIndicator = undefined;
			_this = undefined;
		};
	}

	ss.ohlmpong.PreloadScene = PreloadScene;
}());

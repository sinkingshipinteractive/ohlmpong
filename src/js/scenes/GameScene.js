/* global require, ss, soundsSpriteJSON */

require.include("namespaces/OhlmPongNamespace.js");
require.include("assets/GameAssets");
require.include("config/GameConfig");
require.include("entities/flame.js");
require.include("weblib/asset/AssetLoaderEvent");
require.include("weblib/asset/CreateJSAssetManager");
require.include("weblib/ui/percentdisplays/TextProgressIndicator");
require.include("weblib/ui/percentdisplays/ImageProgressIndicator");
require.include("weblib/logging/Log");
require.include("weblib/scene/AbstractScene");
require.include("weblib/scene/SceneEvent");
require.include("entities/OhlmLayer.js");
require.include("entities/FlameLayer.js");
require.include("entities/NumberDisplay.js");
require.include("entities/CaptionDisplay.js");

(function() {
	"use strict";

	GameScene.prototype = new ss.AbstractScene();
	GameScene.prototype.constructor = GameScene;

	/*
	* Scene containing a preloader.
	* @param gameWidth: [Number] The width of the game in pixels.
	* @param gameHeight: [Number] The height of the game in pixels.
	*/
	function GameScene (gameWidth, gameHeight) {

		// extend abstract scene, and keep a locally scoped copy of this.
		ss.AbstractScene.call(this, gameWidth, gameHeight);

		// keep a locally scoped copy of this.
		var _this = this;

		// List of active meatballs on screen.
		var _balls = [];

		// List of the velocities of the meatballs on screen, in pixels per second.
		var _ballVelocities =[];

		// The player's pan.
		var _playerPan;

		// The position of the player's pan last frame.
		var _lastPlayerX;

		// The acceleration due to gravity in pixels per second per second.
		var _gravity = 750;

		// The time between each meatball.
		var _timeBetweenSpawns = 4;

		// How long until the next meatball spawns.
		var _timeUntilNextSpawn = _timeBetweenSpawns;

		// Display of the score.
		var _scoreBG;
		var _scoreDisplay;
		var _newNumDisplay;

		// The max meatballs that have been on screen.
		var _maxMeatballs = 0;

		// The bowl at the top left of the screen.
		var _cornerBowlLeft;

		// The bowl at the top right of the screne.
		var _cornerBowlRight;

		// The layer that the meatballs are added to.
		var _ballLayer;

		// The background image.
		var _bg;

		// The previous number of meatballs on screen.
		var _prevNum;

		// how long to shake the screen.
		var _shakeTime = 0;

		// The layer containing ohlm.
		var _ohlmLayer;

		var _counterLayer;

		var _flameLayer;

		var _spawns;

		var _splats;

		var _panFront;

		var _tryAgain;

		var _captionDisplay;

		var _tryAgainLines  = [
			{
				"vo": "OT_PP_BeatMallsMouncing",
				"captions": [
					{ text: "Keep those beatmalls mouncing." }
				]
			},
			{
				"vo": "OT_PP_BeatMallsTryAgain",
				"captions": [
					{ text: "All the beatmalls bounced off screen. Let's try again." }
				]
			},
			{
				"vo": "OT_PP_HadToBounce",
				"captions": [
					{ text: "Well, I guess all the meatballs had to bounce. Let's try again." }
				]
			},
			{
				"vo": "OT_PP_KeepEmBouncing",
				"captions": [
					{ text: "Keep those meatballs bouncing!" }
				]
			},
			{
				"vo": "OT_PP_KeepThemInAir",
				"captions": [
					{ text: "Keep the meatballs in the air!" }
				]
			},
			{
				"vo": "OT_PP_MeatBallsTryAgain",
				"captions": [
					{ text: "All the meatballs bounced off screen! Let's try again." }
				]
			}
		];

		/*
		* Start up the scene.
		*/
		_this.startScene = function() {
			_ballVelocities = [];
			_spawns = [];
			_splats = [];

			_bg =  ss.CreateJSAssetManager.getBitmap("Background");
			_this.addChild(_bg);

			_prevNum = 0;

			_captionDisplay = new ss.ohlmpong.CaptionDisplay();
		

			_ohlmLayer = new ss.ohlmpong.OhlmLayer(_captionDisplay);
			_this.addChild(_ohlmLayer);

			_counterLayer = ss.CreateJSAssetManager.getBitmap("CounterTop");
			_counterLayer.y = 768 - _counterLayer.height;
			_this.addChild(_counterLayer);

			_playerPan = ss.CreateJSAssetManager.getBitmap("Pot");
			_playerPan.regX = _playerPan.width / 2;
			_playerPan.regY = _playerPan.height / 2;
			_playerPan.y = 690;
			_playerPan.x = 512;
			_this.addChild(_playerPan);

			createCornerBowls();

			_ballLayer = new createjs.Container();
			_this.addChild(_ballLayer);

			_panFront = ss.CreateJSAssetManager.getBitmap("PotFront");
			_panFront.regX = _panFront.width / 2;
			_panFront.regY = _panFront.height / 2;
			_panFront.y = _playerPan.y - 3;
			_this.addChild(_panFront);

			spawnBall();

			_scoreBG = ss.CreateJSAssetManager.getBitmap("HighScoreBG");
			_scoreBG.regX = _scoreBG.width / 2;
			_scoreBG.x = 512;
			_scoreBG.y = 20;
			_this.addChild(_scoreBG);

			_scoreDisplay =  new ss.ohlmpong.NumberDisplay();
			_scoreDisplay.start(0, "numbers_small", false);
			_scoreDisplay.x = 560;
			_scoreDisplay.y = 25;
			_this.addChild(_scoreDisplay);

			_this.stage.on("stagemousemove", _mouseMove);
			_lastPlayerX = 512;


			_newNumDisplay = new ss.ohlmpong.NumberDisplay();
			_newNumDisplay.start(0, "numbers", true);
			_newNumDisplay.x = 500;
			_newNumDisplay.y = 100;
			_newNumDisplay.scaleX = 0;
			_newNumDisplay.scaleY = 0;
			_this.addChild(_newNumDisplay);

			_tryAgain = ss.CreateJSAssetManager.getBitmap("TryAgain", false, false, false, true);
			_tryAgain.x = 512;
			_tryAgain.y = 384;
			_tryAgain.scaleX = 0;
			_tryAgain.scaleY = 0;
			_this.addChild(_tryAgain);

			_this.addChild(_captionDisplay);
			_captionDisplay.start();

			_captionDisplay.playScript("OT_PP_Intro",
				[
					{text: "Hi, I'm agent Ohlm, and welcome to my pan door!", time: 4500},
					{text: "Move the pying fran, I mean the frying pan, back and forth", time: 4000},
					{text: "to see how many bouncing meatballs you can keep in the air at once.", time: 4000},
					{text: "That's your high score!"}
				]
			);

			ss.AudioManager.playLooped("music-loop");
			ss.AudioManager.setVolume("music-loop", 0.75);
		};

		/**
		 * Create the corner bowls.
		 */
		function createCornerBowls() {
			_cornerBowlLeft = ss.CreateJSAssetManager.getBitmap("Bowl");
			_cornerBowlLeft.regX = _cornerBowlLeft.width / 2;
			_cornerBowlLeft.regY = _cornerBowlLeft.height / 2;
			_cornerBowlLeft.x = 100;
			_cornerBowlLeft.y = 100;
			_cornerBowlLeft.rotation = 135;
			_this.addChild(_cornerBowlLeft);

			_cornerBowlRight = ss.CreateJSAssetManager.getBitmap("Bowl");
			_cornerBowlRight.regX = _cornerBowlRight.width / 2;
			_cornerBowlRight.regY = _cornerBowlRight.height / 2;
			_cornerBowlRight.y = 100;
			_cornerBowlRight.x = 1024 - 100;
			_cornerBowlRight.rotation = -135;
			_this.addChild(_cornerBowlRight);
		}

		/**
		 * Spawn a new meatball.
		 */
		function spawnBall() {
				var _ball = ss.CreateJSAssetManager.getBitmap("Meatball");
				_ball.scaleX = 0.25;
				_ball.scaleY = 0.25;
				_ball.regX = 305;
				_ball.regY = _ball.height / 2;
				_ball.x = 512;
				_ball.y = ss.MathUtils.randomRange(0, 0);

				var _spawnImg = new ss.CreateJSAssetManager.getBitmap("SpawnImage");
				_spawnImg.regX = _spawnImg.width / 2;
				_spawnImg.x = _ball.x;
				_ballLayer.addChild(_spawnImg);
				createjs.Tween.get(_spawnImg).to({alpha: 0}, 750);
				_spawns.push(_spawnImg);

				ss.AudioManager.play("sfx-ball-spawn");
				_ballLayer.addChild(_ball);

				_ballVelocities.push( new ss.Vector2(ss.MathUtils.randomRange(-250, 250), ss.MathUtils.randomRange(0,50) ) );

				_balls.push(_ball);
		}

		/**
		 * Move the mouse.
		 * @param  {MouseEvent} evt - The mouse event.
		 */
		function _mouseMove(evt) {
			_playerPan.x = evt.stageX;
			_panFront.x = _playerPan.x;
		}

		/**
		 * Reflect a vector around a normal.
		 * @param  {ss.Vector2} v - The vector to reflect.
		 * @param  {ss.Vector2} n - The normal to reflect around.
		 * @return {ss.Vector2}   - The reflected vector.
		 */
		function reflect(vector, normal) {
			var d = vector.x * normal.x + vector.y * normal.y;
			return new ss.Vector2((vector.x - 2 * d * normal.x),(vector.y - 2 * d * normal.y));
		}

		/**
		 * Display the effect of a meatball hitting a collider, such as the pan, pots, or edges of the screen.
		 * @param  {Number} x - The x-coordinate of the hit.
		 * @param  {Number} y - The y-coordinate of the hit.
		 */
		function hitEffect(x, y) {
			_shakeTime = 0.1;

			var newSplat = ss.CreateJSAssetManager.getSprite("splat");
			_splats.push(newSplat);
			newSplat.regX = 81;
			newSplat.regY = 45;
			newSplat.x = x;
			newSplat.y = y;
			_ballLayer.addChild(newSplat);
		}

		/**
		 * Check a meatball against the sides of the screen.
		 * @param  {createjs.DisplayObject} _ball         - The meatball.
		 * @param  {ss.Vector2} 			_ballVelocity - The velocity of the meatball.
		 * @param  {Number} 				delta 		  - The time passed since the last frame (in seconds).
		 */
		function _checkBallAgainstSides(_ball, _ballVelocity, delta) {
			if(_ball.x + _ballVelocity.x * delta < 0) {
				_ballVelocity.x *= -1;
				ss.AudioManager.play("sfx-wall-hit");
				hitEffect(_ball.x, _ball.y);
			} else if (_ball.x + _ballVelocity.x * delta  > 1024) {
				_ballVelocity.x *= -1;
				ss.AudioManager.play("sfx-wall-hit");
				hitEffect(_ball.x, _ball.y);
			}
		}

		/**
		 * Check a meatball against the top of the screen.
		 * @param  {createjs.DisplayObject} _ball         - The meatball.
		 * @param  {ss.Vector2} 			_ballVelocity - The velocity of the meatball.
		 * @param  {Number} 				delta 		  - The time passed since the last frame (in seconds).
		 */
		function _checkBallAgainstTop(_ball, _ballVelocity, delta) {
			if(_ball.y + _ballVelocity.y * delta <  0) {
				_ball.y = 0;
				_ballVelocity.y *= -1;
				ss.AudioManager.play("sfx-wall-hit");
				hitEffect(_ball.x, _ball.y);
			}
		}

		/**
		 * Check a meatball against the player's pan (paddle).
		 * @param  {createjs.DisplayObject} _ball         - The meatball.
		 * @param  {ss.Vector2} 			_ballVelocity - The velocity of the meatball.
		 * @param  {Number} 				delta 		  - The time passed since the last frame (in seconds).
		 */
		function _checkBallAgainstPaddle(_ball, _ballVelocity, delta) {
			if(_ball.y + _ballVelocity.y * delta > 670) {
					if(Math.abs (_ball.x - _playerPan.x) < 200) {
						_ball.y = 670;
						var panNormal = new ss.Vector2(-Math.sin(_playerPan.rotation * (Math.PI/180)), Math.cos(_playerPan.rotation * (Math.PI / 180)));
						var newBallVelocity = reflect(_ballVelocity, panNormal);
						_ballVelocity.x = newBallVelocity.x;
						_ballVelocity.y = newBallVelocity.y;
						ss.AudioManager.play("sfx-pan-hit");
						hitEffect(_ball.x, _ball.y);
				}
			}

		}


		/**
		 * Check a meatball against the two bowls.
		 * @param  {createjs.DisplayObject} _ball         - The meatball.
		 * @param  {ss.Vector2} 			_ballVelocity - The velocity of the meatball.
		 * @param  {Number} 				delta 		  - The time passed since the last frame (in seconds).
		 */
		function _checkBallAgainstBowls(_ball, _ballVelocity, delta) {
			var lineX, normal, newVel;

				// TOP LEFT BOWL!
				if(_ball.x + _ballVelocity.x * delta  < 263 && _ball.y + _ballVelocity.y * delta < 263) {
					// if we're in the rectangle of the
					// evaluate the line formula for the top left pan at the ball's y coordinate:
					lineX = -_ball.y + 263;

					if(lineX >= _ball.x) {
						normal = new ss.Vector2(1, 1);
						normal.normalize();

						newVel = reflect(_ballVelocity, normal);

						_ballVelocity.x = newVel.x;
						_ballVelocity.y = newVel.y;
						_ball.x = lineX;
						ss.AudioManager.play("sfx-tray-hit");
						hitEffect(_ball.x, _ball.y);
					}
				}


				// TOP RIGHT BOWL!
				 if(_ball.x + _ballVelocity.x * delta > 725 && _ball.y + _ballVelocity.y * delta < 263) {
					// if we're in the rectangle of the
					// evaluate the line formula for the top left pan at the ball's y coordinate:
					lineX = _ball.y + 761;

					if(lineX <= _ball.x) {
						normal = new ss.Vector2(-1, 1);
						normal.normalize();

						newVel = reflect(_ballVelocity, normal);

						_ballVelocity.x = newVel.x;
						_ballVelocity.y = newVel.y;
						_ball.x = lineX;
						ss.AudioManager.play("sfx-tray-hit");
						hitEffect(_ball.x, _ball.y);
					}
				}
		}

		/**
		 * Check a meatball against the bottom of the screen.
		 * @param  {createjs.DisplayObject} _ball         - The meatball.
		 * @param  {ss.Vector2} 			_ballVelocity - The velocity of the meatball.
		 * @param  {Number} 				delta 		  - The time passed since the last frame (in seconds).
		 * @param  {Number}					i 			  - The index into the ball array.
		 */
		function _checkBallAgainstBottom(_ball, _ballVelocity, delta, i) {
			if(_ball.y + _ballVelocity.y * delta >= 768) {
					ss.AudioManager.play("sfx-miss-out");
					_balls.splice(i, 1);
					_ballVelocities.splice(i,1);
					_ballLayer.removeChild(_ball);
				}
		}

		/**
		 * Check a meatball for collisions against the various parts of the game.
		 * @param  {createjs.DisplayObject} 	_ball         - The meatball.
		 * @param  {ss.Vector2} 				_ballVelocity - The velocity of the meatball, in pixels per second.
		 * @param  {Number} 					delta         - The time passed since the last frame, in seconds.
		 * @param  {Number} 					i             - The index into the meatball array for this meatball.
		 */
		function _checkBallCollisions(_ball, _ballVelocity, delta, i) {
			_checkBallAgainstSides(_ball, _ballVelocity, delta);
			_checkBallAgainstTop(_ball, _ballVelocity, delta);
			_checkBallAgainstPaddle(_ball, _ballVelocity, delta);
			_checkBallAgainstBowls(_ball, _ballVelocity, delta);
			_checkBallAgainstBottom(_ball, _ballVelocity, delta, i);
		}

		/**
		 * Update a meatball's position, rotation and scale.
		 * @param  {createjs.DisplayObject} _ball         - The meatball.
		 * @param  {ss.Vector2} 			_ballVelocity - The velocity of the ball, in pixels per second.
		 * @param  {Number} 				delta         - The time passed since the last frame, in seconds.
		 * @return {[type]}               [description]
		 */
		function _updateBall(_ball, _ballVelocity,  delta, doSauce) {
				_ball.x += _ballVelocity.x * delta;
				_ball.y += _ballVelocity.y * delta;

				_ballVelocity.y += _gravity * delta;

				_ball.rotation = Math.atan2(_ballVelocity.y, _ballVelocity.x) * (180 / Math.PI);

				var scaleFactor = ss.MathUtils.clamp01(_ballVelocity.magnitude() / 6000);
				_ball.scaleX = (1 + scaleFactor) * 0.4;
				_ball.scaleY = (1- scaleFactor) * 0.4;
		}

		/**
		 * Update the high score display.
		 * NOTE: this display will only change if the number of meatballs
		 * currently on screen is greater than the previously stored max number of meatballs.
		 */
		function _updateScoreDisplay() {
			if(_prevNum != _balls.length) {
				_newNumDisplay.setText(_balls.length);
				createjs.Tween.get(_newNumDisplay).to({scaleX: 0.25, scaleY: 0.25}, 100).wait(1000).to({scaleX: 0, scaleY:0}, 50);
				if(_balls.length > _prevNum) {
					ss.AudioManager.play("sfx-ohlm-appear");
					_ohlmLayer.showOhlm();
				}

				if(_balls.length === 0 ) {
					if(ss.AudioManager.getCurrentlyPlayingSoundsByTag("vo").length <= 0) {
						var tryAgainLine = _tryAgainLines[Math.floor(Math.random() * _tryAgainLines.length)];
						_captionDisplay.playScript(tryAgainLine.vo, tryAgainLine.captions);
					}
					createjs.Tween.get(_tryAgain).to({scaleX: 1, scaleY: 1}, 150).wait(1500).to({scaleX: 0, scaleY: 0}, 100);
				}
			}
			if(_balls.length > _maxMeatballs) {
				_maxMeatballs = _balls.length;
				_scoreDisplay.setText(_maxMeatballs);

			}


			_prevNum= _balls.length;
		}

		/**
		 * Spawn a new meatball if necessary, or just count down until the next spawn.
		 * @param  {Number} delta - The time passed since the last frame.
		 */
		function _updateSpawning(delta) {
			_timeUntilNextSpawn -= delta;
			if(_timeUntilNextSpawn <= 0) {
				_timeUntilNextSpawn = _timeBetweenSpawns;
				spawnBall();
			}
		}

		/**
		 * Update the position of the player's pan.
		 * @param  {Number} delta - The time passed since the last frame.
		 */
		function _updatePlayerPan(delta) {
			var playerDelta = _playerPan.x - _lastPlayerX;
			playerDelta *= delta;
			_lastPlayerX = _playerPan.x;
			_playerPan.rotation = ss.MathUtils.lerp1D(_playerPan.rotation, playerDelta * 50, delta * 5);

		}

		function _updateScreenShake(delta) {

			if(_shakeTime > 0) {
				_shakeTime -= delta;
				_this.x = (Math.random() * 3) - 1.5;
				_this.y = (Math.random() * 3) - 1.5;
			} else {
				_this.x = 0;
				_this.y = 0;
			}

		}

		/**
		 * Update the scene.
		 * @param  {Number} delta - The time passed since the last frame, in seconds.
		 */
		_this.updateScene = function (delta) {

			_updateScoreDisplay();
			_updateSpawning(delta);

			for (var i = _balls.length - 1; i >= 0 ; i--) {
				var _ball = _balls[i];
				var _ballVelocity = _ballVelocities[i];

				_checkBallCollisions(_ball, _ballVelocity, delta, i);
				_updateBall(_ball, _ballVelocity, delta, false);
			}

			for (i = _spawns.length -1; i >= 0; i--) {
				if(_spawns[i].alpha <= 0.01) {
					_ballLayer.removeChild(_spawns[i]);
					_spawns.splice(i, 1);
				}
			}

			for (i = _splats.length -1; i >= 0; i--) {
				if(_splats[i].currentFrame > 10) {
					_ballLayer.removeChild(_splats[i]);
					_splats.splice(i, 1);
				}
			}

			_updatePlayerPan(delta);

			_panFront.x = _playerPan.x;
			_panFront.rotation = _playerPan.rotation;

			_updateScreenShake(delta);

			_ohlmLayer.update();

			//_flameLayer.update(delta)
		};

		/*
		* Destroy the scene.
		*/
		_this.destroyScene = function() {
			_this.release();
		};

		/**
		 * Release any assets held by this scene.
		 */
		this.release = function() {
			_this = undefined;
		};
	}

	ss.ohlmpong.GameScene = GameScene;
}());


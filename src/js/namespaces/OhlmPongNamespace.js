/* global require, ss */
require.include("weblib/core/Util");
require.include("weblib/ssnamespace");

(function() {
	"use strict";

	// Declare the ohlmpong namespace object if it hasn't already been defined.
	if ( ss.isUndefined(ss.ohlmpong) ) {
		ss.ohlmpong = {};
	}
} ());
